My name is Andrea Stella.\
I am graduating in Computer Science, I have a passion for Data Science and Machine Learning
(this is my ideal job position in future) other than Cyber Security and DIY.

I have always been curious about how things are done, especially regarding Hardware and Software.
I think this is my greatest strength point. I am always curious about something.
Right now, I am working with Docker and Docker-Compose and I have discovered to be a sucker for containers and containers orchestration systems!

Currently I am working in an internship project as a backend developer.
I work in a team, using Git as our software versioning control system. 
Thanks to this experience, my confidence with Git has increased a lot. 
In fact, now I use regularly Git for my personal projects. 

I love building things, making software design (and implementation) one of my favourite
free time job other than a constantly evolving skill I share with some of my colleagues and friends.
