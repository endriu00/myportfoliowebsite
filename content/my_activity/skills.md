I have worked with many programming languages and tools.\
Here are some:
- Go
- Python
- SQL (MySQL, PostgreSQL)
- Java
- Git
- GitHub
- Docker
- Docker-compose
- Linux
- Raspberry Pi
- C
- C++
- UML
- Marvel
- Modelica
